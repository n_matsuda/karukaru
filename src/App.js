import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      count : 5
    }
  }

  plus() {
    this.setState({
      count : this.state.count * 2
    })
  }

  minus() {
    this.setState({
      count : this.state.count - 1
    })
  }

  render() {
    return (
      <span>
        <div>
          count : {this.state.count}
        </div>

        <div>
          <button onClick={this.plus.bind(this)}>
          	<body bgcolor="#008080">
          		kakeru
          	</body>
          </button>
          
          <button onClick={this.minus.bind(this)}>
          	<body bgcolor="#ff0000">
          		Minus
          	</body>
          </button>
        </div>
      </span>
    )
  }
}

export default App;

